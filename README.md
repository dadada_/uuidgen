# UuidGen

UuidGen is a simple UUID Generator for Android. It was created as a personal learning experience. It supports copying to clipboard and displays a history of previously generated UUIDs.

## License

See [LICENSE](LICENSE)
