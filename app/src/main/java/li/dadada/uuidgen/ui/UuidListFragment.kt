package li.dadada.uuidgen.ui

import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import li.dadada.uuidgen.R
import li.dadada.uuidgen.model.Uuid
import li.dadada.uuidgen.viewmodel.UuidAdapter
import li.dadada.uuidgen.viewmodel.UuidViewModel
import li.dadada.uuidgen.databinding.UuidListFragmentBinding
import li.dadada.uuidgen.viewmodel.ClipBoardUtil
import li.dadada.uuidgen.viewmodel.OnUuidClickListener


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class UuidListFragment : Fragment() {
    private val uuidAdapter = UuidAdapter(object : OnUuidClickListener {
        override fun onUuidClicked(view: View, uuid: Uuid): Boolean {
            ClipBoardUtil.copyUuid(
                uuid,
                view,
                activity?.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager)
            return true
        }
    })

    private lateinit var binding : UuidListFragmentBinding

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        binding = DataBindingUtil.inflate(inflater, R.layout.uuid_list_fragment, container, false)
        binding.listUuids.apply {
            adapter = uuidAdapter
            layoutManager = LinearLayoutManager(this@UuidListFragment.context)
        }

        return binding.root;
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_list, menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).supportActionBar?.title =
                getString(R.string.title_history)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_to_creation -> {
                findNavController().navigate(R.id.action_global_uuid_gen_fragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(this).get(UuidViewModel::class.java)
        viewModel.uuidList.observe(viewLifecycleOwner,
            Observer<List<Uuid>> { list -> uuidAdapter.submitList(list) })
    }

}