package li.dadada.uuidgen.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Uuid::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun uuidDao(): UuidDao
}
