package li.dadada.uuidgen.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UuidDao {
    @Query("SELECT * FROM `uuid` ORDER BY `created` ASC")
    fun getAll(): LiveData<List<Uuid>>

    @Query("SELECT * FROM `uuid` WHERE `uuid` IS :uuid")
    fun get(uuid: String) : List<Uuid>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(vararg uuid: Uuid)

    @Delete
    fun delete(user: Uuid)
}
