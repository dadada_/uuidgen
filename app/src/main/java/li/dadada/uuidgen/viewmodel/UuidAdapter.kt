package li.dadada.uuidgen.viewmodel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import li.dadada.uuidgen.R
import li.dadada.uuidgen.databinding.UuidItemBinding
import li.dadada.uuidgen.model.Uuid


class UuidAdapter(private val controller: OnUuidClickListener) : ListAdapter<Uuid, UuidAdapter.UuidViewHolder>(UuidDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UuidViewHolder {
        return from(parent, controller)
    }

    override fun onBindViewHolder(holder: UuidViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class UuidViewHolder(
        private val binding: UuidItemBinding,
        private val listener: OnUuidClickListener
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Uuid) {
            binding.uuid = item
            binding.listener = listener
            binding.executePendingBindings()
        }
    }

    companion object {
        fun from(parent: ViewGroup, listener: OnUuidClickListener) : UuidViewHolder {
            val binding : UuidItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.uuid_item,
                parent, false
            )
            return UuidViewHolder(binding, listener)
        }
    }
}

class UuidDiffCallback : DiffUtil.ItemCallback<Uuid>() {
    override fun areItemsTheSame(oldItem: Uuid, newItem: Uuid): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Uuid, newItem: Uuid): Boolean {
        return oldItem == newItem
    }
}