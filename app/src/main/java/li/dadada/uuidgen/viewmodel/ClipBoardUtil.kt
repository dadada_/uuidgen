package li.dadada.uuidgen.viewmodel

import android.content.ClipData
import android.content.ClipboardManager
import android.view.View
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import li.dadada.uuidgen.model.Uuid

object ClipBoardUtil {
    fun copyUuid(uuid: Uuid, view: View, clipboardManager: ClipboardManager) {
        clipboardManager.setPrimaryClip(ClipData.newPlainText("uuid", uuid.uuid))
        Snackbar.make(view, "Copied $uuid to clipboard", BaseTransientBottomBar.LENGTH_SHORT)
            .show()
    }
}