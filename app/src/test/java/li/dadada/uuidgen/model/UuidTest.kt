package li.dadada.uuidgen.model

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class UuidTest {

    @Test
    fun `random UUID are not equal`() {
        val u1 = Uuid()
        val u2 = Uuid()
        assertFalse(u1 == u2)
    }

    @Test
    fun `UUIDs with same id are equal`() {
        val u1 = Uuid()
        val u2 = Uuid(uuid = u1.uuid)
        assertTrue(u1 == u2)
    }

    @Test
    fun `UUID is self identical`() {
        val u1 = Uuid()
        assertTrue(u1 == u1)
    }
}